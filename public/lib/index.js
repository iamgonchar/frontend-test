(function() {
    const MODAL_CLASS = "modal";
    const CONTENT_CLASS = "modal__content";
    const FRAME_WRAPPER_CLASS = "modal__frame-wrapper";
    const FRAME_CLASS = "modal__frame";
    const CLOSE_BUTTON_CLASS = "modal__close-button";

    /**
     * Метод создания контейнера
     * */
    const createWrap = (elementId) => {
        const modalWrap = document.createElement("div");
        modalWrap.className = MODAL_CLASS;
        modalWrap.id = `${elementId}-modal`;

        return modalWrap;
    };

    /**
     * Метод создания блока контента
     * */
    const createContent = () => {
        const content = document.createElement("div");
        content.className = CONTENT_CLASS;

        return content;
    };

    /**
     * Метод создания фрейма с видео
     * */
    const createFrame = (src) => {
        const frameWrapper = document.createElement("div");
        frameWrapper.className = FRAME_WRAPPER_CLASS;

        const frame = document.createElement("iframe");
        frame.src = src;
        frame.frameBorder = 0;
        frame.allowFullscreen = true;
        frame.className = FRAME_CLASS;

        frameWrapper.appendChild(frame);

        return frameWrapper;
    };

    /**
     * Метод создания кнопки удаления
     * */
    const createCloseButton = () => {
        const closeButton = document.createElement("a");
        closeButton.innerText = "x";
        closeButton.href = "#";
        closeButton.className = CLOSE_BUTTON_CLASS;

        return closeButton;
    };


    /**
     * Компонент модалки. Структура:
     *  --modal
     *      |--content
     *          |--close-button
     *          |--frame-wrapper
     *              |--frame
     * */
    class Modal {

        _src = "";
        _elementId = "";
        _modal = null;

        /**
         * @param elementId - уникальный идентификатор кнопки создания
         * @param src - ссылка на видео
         * */
        constructor(elementId, src) {
            this._elementId = elementId;
            this._src = src;
        }

        /**
         * Хэндлер клика по таргет-кнопке.
         * Рендерим модалку + инитим слушатели ескейпа или клика кнопки "Х" для закрытия модалки
         * */
        handleModalOpen = () => {
            const modal = createWrap(this._elementId);
            const content = createContent();
            const closeButton = createCloseButton();
            const frame = createFrame(this._src);

            content.appendChild(closeButton);
            content.appendChild(frame);
            modal.appendChild(content);

            document.body.appendChild(modal);

            closeButton.addEventListener("click", this.handleModalClose);

            window.addEventListener("keydown", ({key}) => {
                if (key === "Escape") {
                    this.handleModalClose();
                }
            });

            this._modal = modal;
        };

        /**
         * Хэндлер закрытия модалки.
         * Здесь удаляем модалку из ДОМа и снимаем глобальный слушатель нажатий
         * */
        handleModalClose = () => {
            window.removeEventListener("keydown", this.handleModalOpen);

            this._modal.remove();
        };

        /**
         * Метод инициализации слушателя кнопки открытия модалки.
         * */
        init() {
            const targetButton = document.getElementById(this._elementId);
            targetButton.addEventListener("click", this.handleModalOpen);
        }

        /**
         * Метод удаления модалки и сноса слушателя с кнопки
         * */
        destroy() {
            this.handleModalClose();

            const targetButton = document.getElementById(this._elementId);
            targetButton.removeEventListener("click", this.handleModalOpen);
        }
    }

    window.Modal = Modal;
}());