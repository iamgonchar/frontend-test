window.onload = () => {
    const config = {
        rootMargin: '0px',
        threshold: 0
    };

    let observer = new IntersectionObserver((entries, self) => {
        entries.forEach(entry => {
            if(entry.isIntersecting) {
                entry.target.src = entry.target.dataset.src;
                self.unobserve(entry.target);
            }
        });
    }, config);

    const images = document.querySelectorAll('[data-src]');
    images.forEach(img => {
        observer.observe(img);
    });
};